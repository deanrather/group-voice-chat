# Group Voice Chat

## Purpose

- 

## Usage Scenarios

- skiing in a group (can't use hands, easy to lose people0
- working (saves physically standin up)
- going to a festival (easy to get lost)

## Requirements

- run's in background (& prevents phone from locking)
- uses very little battery (can run for 8+hrs w/o killing battery)
- doesn't require physical interaction
- works on iphone and android
- auto-reconnects w/ internet

## Nice to haves

- dims music
- p2p (no server costs)
- voice recognition ((un)mute)
- chat notifications (adi had joined|left|muted the chat)
- system notifications (the time is 5pm, you have 1h battery remaining)
- friend locator (where is <adi>: 500meters north-west)
- auto-voice catchup like apple chat

## Research

### Technologies

- js p2p
- web voice recognition

### Similar Applications

- Discord (definitly worth looking into -- not sure if mobile app can run in background)
- TeamSpeak (nonfree, pc-only)
- Google Hangounts
- Skype